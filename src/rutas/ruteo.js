'use strict';

(function () {
    function init() {
        var router = new Router([
            new Route('home', 'inicio.html', true),
            new Route('catcilist', 'categocitas/catecitalistar.html'),
            new Route('catcitaadd', 'categocitas/catecitacrear.html'),
            new Route('catcimanage', 'categocitas/catecitaadmin.html'),
            new Route('catcidelete', 'categocitas/catecitaeliminar.html'),
            new Route('catciupdate', 'categocitas/catecitaactualizar.html')
        ]);
    }
    init();
}());
