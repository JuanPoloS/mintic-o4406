function actualizarCateCita() {
    const codigo = document.getElementById("cod").value;
    const nombre = document.getElementById("nom").value;
    if (nombre !== "") {
      let objetoEnviar = {
        codCategocita: codigo,
        tipoCita: nombre
      };
  
      const apiCrear = "http://localhost:8094/pryconsultorio/categocita/actualizar";
      fetch(apiCrear, {
        method: "PUT",
        body: JSON.stringify(objetoEnviar),
        headers: { "Content-type": "application/json; charset=UTF-8" },
      })
        .then((respuesta) => respuesta.json())
        .then((datos) => {
          if (datos.error == "Accepted") {
            console.log(datos);
            document.getElementById("rolMsgOk").style.display = "";
            document.getElementById("rolMsgError").style.display = "none";
          } else {
            document.getElementById("rolMsgOk").style.display = "none";
            document.getElementById("rolMsgError").style.display = "";
          }
        })
        .catch((miError) => {
          console.log(miError);
        });
    }
  }