function capturarRolesAdmin() {
  const apiListaRol = "http://localhost:8094/pryconsultorio/categocita/obtener";
  const miPrimeraPromesa = fetch(apiListaRol).then((resultado) =>
    resultado.json()
  );

  Promise.all([miPrimeraPromesa]).then((arregloDeDatos) => {
    const misDatos = arregloDeDatos[0];
    agregarFilasAdmin(misDatos);
  });
}

function agregarFilasAdmin(arregloExterno) {
  const cantidad = arregloExterno.length;
  for (let i = 0; i < cantidad; i++) {
    const codigo = arregloExterno[i].codCategocita;
    const nombre = arregloExterno[i].tipoCita;

    document.getElementById("tablaRolesAdmin").insertRow(-1).innerHTML =
      "<td>" + codigo + "</td>" + "<td>" + nombre + "</td>" + 
      "<td> <a href = 'javascript:confirmarBorrarRol("+ codigo + ");'<i class='fa-solid fa-trash check-rojo'></i></a>&nbsp;" + 
      "<a href = '#catciupdate/" + codigo + "'><i class='fa-solid fa-pen-to-square check-verde'></i></a>" + "</td>";
  }
}

function confirmarBorrarRol(codigo){
  if (window.confirm("Seguro que quiere eliminar el rol seleccionado")){
    window.location.replace("#catcidelete/"+codigo);
  }

}