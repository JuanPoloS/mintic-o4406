function capturarRoles() {
  const apiListaRol = "http://localhost:8094/pryconsultorio/categocita/obtener";
  const miPrimeraPromesa = fetch(apiListaRol).then((resultado) =>
    resultado.json()
  );

  Promise.all([miPrimeraPromesa]).then((arregloDeDatos) => {
    const misDatos = arregloDeDatos[0];
    agregarFilas(misDatos);
  });
}

function agregarFilas(arregloExterno) {
  const cantidad = arregloExterno.length;
  for (let i = 0; i < cantidad; i++) {
    const codigo = arregloExterno[i].codCategocita;
    const nombre = arregloExterno[i].tipoCita;

    document.getElementById("tablaRoles").insertRow(-1).innerHTML =
      "<td>" + codigo + "</td>" + "<td>" + nombre + "</td>";
  }
}
