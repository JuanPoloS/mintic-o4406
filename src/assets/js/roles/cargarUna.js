function obtenerRolActualizar(codigo) {
  const apiListaRol =
    "http://localhost:8094/pryconsultorio/categocita/buscando/" + codigo;
  const miPrimeraPromesa = fetch(apiListaRol).then((resultado) =>
    resultado.json().then((dato) => {
      if (dato.hasOwnProperty("codCategocita")) {
        document.getElementById("cod").value = dato.codCategocita;
        document.getElementById("nom").value = dato.tipoCita;
      } else {
      }
    })
  );

  Promise.all([miPrimeraPromesa]).then((arregloDeDatos) => {
    const misDatos = arregloDeDatos[0];
    agregarFilas(misDatos);
  });
}
